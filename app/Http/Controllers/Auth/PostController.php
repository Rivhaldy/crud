<?php

namespace App/Http/Controllers;

use Illuminate/Http/Request;
use DB;

class PostController extends Controllers{
	public function create(){
		return view('Post.create');
	}


	public function store(Request $rrequest){
	$request->validate([
		'title' => 'required|unique:Post',
		'body' => 'requir'
	]);
	$query = DB::table('Post')->insert([
		"title" => $request ["title"],
		"body" => $request ["body"]
	]);

	return redirect('/Post/create')->with("succes", "Data berhasil ditambahkan");
	}
 

	public function index()
	{
		$Post = DB::table('Post')->get();
		return view('Post.index', compact('Post'));
	}

	public function show($id)
	{
		$Post = DB::table('Post')->where('id', $id)->first();
		return view('Post.show', compact('Post'));
	}

	public function edit($id)
	{
		$Post = DB::table('Post')->where('id', $id)->first();
		return view ('Post.edit', compact('Post'));
	}
	public function update ($id, Request $request)
	{
		$request->validate([
			'title' => 'required|unique:Post',
			'body' => 'required'
		]);

	 $query = DB::table('Post')
            ->where('id', $id)
            ->update([
                'title' => $request["title"],
                'body' => $request["body"]
            ]);
        return redirect('/p')->with("success", "Data Berhasil Diupdate");;
    }

    public function destroy($id)
    {
        $query = DB::table('Post')->where('id', $id)->delete();
        return redirect('/Post')->with("success", "Data Berhasil Dihapus");;




	}
}