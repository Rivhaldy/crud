<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Post', 'postController@index');
Route::post('Post/create', 'postController@create');
Route::post('/Post', 'PostController@store');
Route::get('Post', 'PostController@show');
Route::get('/Post/edit', 'PostController@edit');
Route::put('/Post', 'PostController@update');
Route::delete('/Post', 'PostController@destroy');
